import React, { Component } from 'react';
import Embed from '../components/powerbi-component/component';

import { AuthenticationContext } from 'react-adal';
import PowerbiEmbedded from 'react-powerbi';
import * as pbi from 'powerbi-client';

import { runWithAdal } from 'react-adal';

const adalConfig = {
  tenant: 'aspiresysinc.onmicrosoft.com',
  clientId: '16f7a751-c97c-4ddb-9612-bf079b0c9926',
  endpoints: {
    api: 'https://aspiresysinc.onmicrosoft.com/16f7a751-c97c-4ddb-9612-bf079b0c9926'
  },
  postLogoutRedirectUri: window.location.origin,
  redirectUri: 'http://localhost:13526/Redirect',
  cacheLocation: 'sessionStorage'
 };
 const authContext = new AuthenticationContext(adalConfig);
 const getToken = () => {
  return authContext.getCachedToken(authContext.config.clientId);
 };

export default class extends Component {
  constructor(props) {
    super(props);
/*react-powerbi*/
    this.onEmbedded = this.onEmbedded.bind(this);
    this.toggleFilterPaneClicked = this.toggleFilterPaneClicked.bind(this);
    this.toggleNavContentPaneClicked = this.toggleNavContentPaneClicked.bind(this);
    this.report = null;
    this.filterPaneEnabled = false;
    this.navContentPaneEnabled = false;

    this.powerbi = new pbi.service.Service(pbi.factories.hpmFactory, pbi.factories.wpmpFactory, pbi.factories.routerFactory);

    this.state = {
      embedConfig: null,
      authContext: []
    };
  }
  componentDidMount() {
     console.log(pbi)
    let config = {
      accessToken: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ2ZXIiOiIwLjIuMCIsIndjbiI6IlBvd2VyQmlBenVyZVNhbXBsZXMiLCJ3aWQiOiJmODFjMTk2Ni1lZGVlLTQxMWItOGY4YS1mODQ0NjAxOWIwNDQiLCJyaWQiOiJjNTJhZjhhYi0wNDY4LTQxNjUtOTJhZi1kYzM5ODU4ZDY2YWQiLCJpc3MiOiJQb3dlckJJU0RLIiwiYXVkIjoiaHR0cHM6Ly9hbmFseXNpcy53aW5kb3dzLm5ldC9wb3dlcmJpL2FwaSIsImV4cCI6MTUzNDUyOTE1NSwibmJmIjoxNTM0NTI1NTU1fQ.oC6eMJ3WNTpNDE0GnWCsjcDXIaodLBWssg9qHR9XWGQ',
      embedUrl:  'https://app.powerbi.com/reportEmbed?reportId=c52af8ab-0468-4165-92af-dc39858d66ad',
      id: 'c52af8ab-0468-4165-92af-dc39858d66ad',
      type:'report',
      tokenType: pbi.models.TokenType.Embed,
      permissions: pbi.models.Permissions.All,
      settings: {
        filterPaneEnabled: true,
        navContentPaneEnabled: true
        }        
    };
    let powerbi = new pbi.service.Service(pbi.factories.hpmFactory, pbi.factories.wpmpFactory, pbi.factories.routerFactory);  

    // Embed the report and display it within the div container.
    var reportContainer = document.getElementById('reportContainer');
    var report = powerbi.embed(reportContainer, config);

      console.log(config);
      this.powerbi.embed(this.embedContainer, config);

    fetch(`https://powerbiembedapi.azurewebsites.net/api/reports/c52af8ab-0468-4165-92af-dc39858d66ad`)
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      else {
        throw new Error(response.json());
      }
    })
    .then(embedConfig => {
      // embedConfig.accessToken ="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjdfWnVmMXR2a3dMeFlhSFMzcTZsVWpVWUlHdyIsImtpZCI6IjdfWnVmMXR2a3dMeFlhSFMzcTZsVWpVWUlHdyJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNDA2ZjZmYjItZTA4Ny00ZDI5LTk2NDItODE3ODczZmRkYzRjLyIsImlhdCI6MTUzNDQwMjg4NywibmJmIjoxNTM0NDAyODg3LCJleHAiOjE1MzQ0MDY3ODcsImFjciI6IjEiLCJhaW8iOiI0MkJnWVBDdXIzUHRYRkF1TmFQcThwZkV0cmE0ZlNKcWI2WWNMRlpaWHFMdXM2bG90VDBBIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6Ijg3MWMwMTBmLTVlNjEtNGZiMS04M2FjLTk4NjEwYTdlOTExMCIsImFwcGlkYWNyIjoiMiIsImVfZXhwIjoyNjI4MDAsImZhbWlseV9uYW1lIjoiU3VicmFtYW5pYW4iLCJnaXZlbl9uYW1lIjoiU2FudGhvc2giLCJpcGFkZHIiOiI2MS4xMi4zNS4xMTUiLCJuYW1lIjoiU2FudGhvc2ggU3VicmFtYW5pYW4iLCJvaWQiOiJkYTc1YjFiZS05MGZmLTQzYzktYTRjMy01NDZjM2M2MWM0YjMiLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzc0NDIzODUzMi01OTA4NzMxNzYtNDE3MjMxOTU4LTE0ODM0OTMiLCJwdWlkIjoiMTAwMzdGRkU5MzE4OUIzNyIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6Im1FMGpkQ2xhUGpIWXFBaGZyNXBoTVdTVHE4OTdCRGRlVFc4NzRwU1gxOGsiLCJ0aWQiOiI0MDZmNmZiMi1lMDg3LTRkMjktOTY0Mi04MTc4NzNmZGRjNGMiLCJ1bmlxdWVfbmFtZSI6InNhbnRob3NoLnN1YnJhbWFuaWFuQEFzcGlyZXN5cy5jb20iLCJ1cG4iOiJzYW50aG9zaC5zdWJyYW1hbmlhbkBBc3BpcmVzeXMuY29tIiwidXRpIjoiTUV6VmhZSWRRVVdFa0pyQmszMkNBQSIsInZlciI6IjEuMCJ9.n90Qk9Ec_KbmO1QBFj8JSBC2udRW7jDH000YTOi9f0EGVL5jb8LKTUQPMGWT7SlmPg56QdqKgFFNqVZhIs6x5IWYBd0UtEzAgNQnlzAb9WnHpi5yedgSBkSafp-sHeqkuJAaNimk_4Rbsw2lV1m9xKSAD0sK_YW2PH3TSUcV7coDAspCeVfBIIBHmGE_0DLNHjnrhSTJSIY9Yf38GP8rhtg2OnbUp7nlTrhp7QbZc2_6adPJj3BUQpvx-Xurf9rCH1oGclYamlepTuSeiUDhNA-FTneEG7vtBAIvkzS-SZZgsIzNH9FVjA60yDxUYl1RiVfFiS-mYcB7JmLXRCqZNQ"
      // embedConfig.embedUrl="https://app.powerbi.com/reportEmbed?reportId=da5d0057-9c3d-4ae1-a8fd-9be1b1cedab4"
      // embedConfig.id="da5d0057-9c3d-4ae1-a8fd-9be1b1cedab4"
      // embedConfig.webUrl = "https://app.powerbi.com/sharedwithme/dashboards/da5d0057-9c3d-4ae1-a8fd-9be1b1cedab4"
      // embedConfig.filterPaneEnabled=true
      // embedConfig.navContentPaneEnabled=false
      this.setState({
        embedConfig
      });
      //console.log(embedConfig)
      return embedConfig;
    });

    //adal
    runWithAdal(authContext, () => {
      // TODO : continue your process
      var token = this.getToken();
      console.log(token)
     });
  }


  onEmbedded(embed) {

    this.report = embed;
  }

  toggleFilterPaneClicked() {
    console.log('toggleFilterPaneClicked');
    this.filterPaneEnabled = !this.filterPaneEnabled;
    this.report.updateSettings({
        filterPaneEnabled: this.filterPaneEnabled
    });
  }

  toggleNavContentPaneClicked() {
    console.log('toggleNavContentPaneClicked');
    this.navContentPaneEnabled = !this.navContentPaneEnabled;
    this.report.updateSettings({
        navContentPaneEnabled: this.navContentPaneEnabled
    });
  }

  render() { 
    
    // if(this.state.authContext !== null) {
    //   console.log(this.state.authContext.getCachedToken('16f7a751-c97c-4ddb-9612-bf079b0c9926'));
    // }
    
    // if(this.state.authContext && this.state.authContext.config.clientId) {
    //     console.log(this.state.authContext.getCachedToken(this.state.authContext.config.clientId))
    // }

    return (
      <div>
        <h1>Update Settings</h1>
        <p>Change visibility of filter pane or page navigation dynamically</p>
        <div id="reportContainer"></div>
        <div className="embedContainer" style={{'height' : '600px', 'width' : '100%'}} ref={(div) => { if (div) {this.embedContainer = div; }}}/>
      

        <button type="button" className="btn btn-primary" onClick={this.toggleFilterPaneClicked}>Toggle Filter Pane</button>
        <button type="button" className="btn btn-primary" onClick={this.toggleNavContentPaneClicked}>Toggle Page Navigation</button>
      </div>
    );
  }
}
