import React, { Component } from 'react';
import Embed from '../components/powerbi-component/component';
import FilterPane from '../components/powerbi-filter-pane/component';

export default class extends Component {
  constructor(props) {
    super(props);

    this.state = {
      embedConfig: {}
    };
  }

  componentDidMount() {
    fetch(`https://powerbiembedapi.azurewebsites.net/api/reports/c52af8ab-0468-4165-92af-dc39858d66ad`)
      .then(response => {
        if (response.ok) {
          return response.json();
        }
        else {
          throw new Error(response.json());
        }
      })
      .then(embedConfig => {
        // embedConfig.accessToken ="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IjdfWnVmMXR2a3dMeFlhSFMzcTZsVWpVWUlHdyIsImtpZCI6IjdfWnVmMXR2a3dMeFlhSFMzcTZsVWpVWUlHdyJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLndpbmRvd3MubmV0L3Bvd2VyYmkvYXBpIiwiaXNzIjoiaHR0cHM6Ly9zdHMud2luZG93cy5uZXQvNDA2ZjZmYjItZTA4Ny00ZDI5LTk2NDItODE3ODczZmRkYzRjLyIsImlhdCI6MTUzNDQwMjg4NywibmJmIjoxNTM0NDAyODg3LCJleHAiOjE1MzQ0MDY3ODcsImFjciI6IjEiLCJhaW8iOiI0MkJnWVBDdXIzUHRYRkF1TmFQcThwZkV0cmE0ZlNKcWI2WWNMRlpaWHFMdXM2bG90VDBBIiwiYW1yIjpbInB3ZCJdLCJhcHBpZCI6Ijg3MWMwMTBmLTVlNjEtNGZiMS04M2FjLTk4NjEwYTdlOTExMCIsImFwcGlkYWNyIjoiMiIsImVfZXhwIjoyNjI4MDAsImZhbWlseV9uYW1lIjoiU3VicmFtYW5pYW4iLCJnaXZlbl9uYW1lIjoiU2FudGhvc2giLCJpcGFkZHIiOiI2MS4xMi4zNS4xMTUiLCJuYW1lIjoiU2FudGhvc2ggU3VicmFtYW5pYW4iLCJvaWQiOiJkYTc1YjFiZS05MGZmLTQzYzktYTRjMy01NDZjM2M2MWM0YjMiLCJvbnByZW1fc2lkIjoiUy0xLTUtMjEtMzc0NDIzODUzMi01OTA4NzMxNzYtNDE3MjMxOTU4LTE0ODM0OTMiLCJwdWlkIjoiMTAwMzdGRkU5MzE4OUIzNyIsInNjcCI6InVzZXJfaW1wZXJzb25hdGlvbiIsInN1YiI6Im1FMGpkQ2xhUGpIWXFBaGZyNXBoTVdTVHE4OTdCRGRlVFc4NzRwU1gxOGsiLCJ0aWQiOiI0MDZmNmZiMi1lMDg3LTRkMjktOTY0Mi04MTc4NzNmZGRjNGMiLCJ1bmlxdWVfbmFtZSI6InNhbnRob3NoLnN1YnJhbWFuaWFuQEFzcGlyZXN5cy5jb20iLCJ1cG4iOiJzYW50aG9zaC5zdWJyYW1hbmlhbkBBc3BpcmVzeXMuY29tIiwidXRpIjoiTUV6VmhZSWRRVVdFa0pyQmszMkNBQSIsInZlciI6IjEuMCJ9.n90Qk9Ec_KbmO1QBFj8JSBC2udRW7jDH000YTOi9f0EGVL5jb8LKTUQPMGWT7SlmPg56QdqKgFFNqVZhIs6x5IWYBd0UtEzAgNQnlzAb9WnHpi5yedgSBkSafp-sHeqkuJAaNimk_4Rbsw2lV1m9xKSAD0sK_YW2PH3TSUcV7coDAspCeVfBIIBHmGE_0DLNHjnrhSTJSIY9Yf38GP8rhtg2OnbUp7nlTrhp7QbZc2_6adPJj3BUQpvx-Xurf9rCH1oGclYamlepTuSeiUDhNA-FTneEG7vtBAIvkzS-SZZgsIzNH9FVjA60yDxUYl1RiVfFiS-mYcB7JmLXRCqZNQ"
        // embedConfig.embedUrl="https://app.powerbi.com/reportEmbed?reportId=da5d0057-9c3d-4ae1-a8fd-9be1b1cedab4"
        // embedConfig.id="da5d0057-9c3d-4ae1-a8fd-9be1b1cedab4"
        // embedConfig.webUrl = "https://app.powerbi.com/sharedwithme/dashboards/da5d0057-9c3d-4ae1-a8fd-9be1b1cedab4"
        // embedConfig.filterPaneEnabled=true
        // embedConfig.navContentPaneEnabled=false
        this.setState({
          embedConfig
        });
        console.log(embedConfig)
        return embedConfig;
      });
  }

  onEmbedded(embed) {
    //console.log(`Report embedded: `, embed, this);
  }

  render() {
    return (
      <div>
        <h1>Custom Filter Pane</h1>
        <p>Filter pane is hidden in the embedded report and recreated by developer to allow custom branding or special preconfigured filters.</p>

        <div className="row">
          <div className="col-md-9">
            <div className="row">
              <div className="col-xs-12">
                <Embed
                  options={this.state.embedConfig}
                  onEmbedded={this.onEmbedded}
                >
                </Embed>
              </div>
            </div>
            <div className="row">
              Pre-defined Filter Buttons (Not Implemented)
            </div>
          </div>
          <div className="col-md-3">
            <FilterPane></FilterPane>
          </div>
        </div>
      </div>
    );
  }
}
